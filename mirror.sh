#!/bin/bash

cwd=`pwd`
 USER=dc16-kandy
 PASS=12345678x@X # space to void this getting into history. If there are better options to anonymize the password I would like to know

# source: https://stackoverflow.com/a/51142042
for i in {1..2}; do
	curl --user $USER:$PASS "https://api.bitbucket.org/2.0/repositories/$USER?pagelen=100&page=${i}" > repoinfo.page${i}.json
done

## [0].href will return the https link and [1].href the ssh link
cat repoinfo.*.json | jq -r '.values[] | .links.clone[1].href' > repos.txt

# source: https://stackoverflow.com/questions/40429610/bitbucket-clone-all-team-repositories
for repo in `cat repos.txt`; do
	projectname=`basename ${repo}`
	if [[ $repo == *".git" ]]; then
		echo "It's a git!"
		command="git"
	else
		echo "It's a mercurial!"
		command="hg"
	fi

	if [ -d "$projectname" ]; then
		echo "Project directory exists. $repo will be updated"
		cd $cwd/$projectname
		$command pull
		cd $cwd
	else
		echo "Cloning" $repo
		$command clone $repo
	fi
done